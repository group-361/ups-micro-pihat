MicroPiHat Uninterrupted Power Supply (UPS)
===========================================

 [                Quick Start Guide](https://learn.pi-supply.com/make/pijuice-zero-quick-start-guide/#pijuice-zero-quick-start-guide)
=====================================================================================================================================

 

The microPiHat (UPS) is a great addition to the Raspberry PiZero.The size of the
microPiHat is compact such that portability is easy.The size of the microPiHat
does not impact the functionality of the Raspberry PiZero because the UPS is
designed in a way that maximizes the battery voltage supply for power back up.

 

[Board Assembly](https://learn.pi-supply.com/make/pijuice-zero-quick-start-guide/#board-assembly)
-------------------------------------------------------------------------------------------------

 

**Step 1** – Unpack your Raspberry PiZero. Make sure that your Pi Zero has a 40
way male header attached.

**Step 2** – Before you add the microPiHat UPS to your Raspberry Pi Zero you
will need to insert the stand off pins to the Raspberry Pi Zero. It is much
easier to do it prior to inserting the microPiHat UPS . Screw the stand off pins
to the Raspberry Pi Zero from the bottom of the board.

**Step 3** – Insert the microPiHat UPS on to the Raspberry Pi Zero and then
secure with the remaining screws.

**Step 4** – Connect the battery to the microPiHat UPS then check if everything
is fully secured and you are good to go.

**Step 5** – Switch on the Raspberry PiZero and continue to use to perform
everyday tasks.
