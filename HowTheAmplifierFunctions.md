# How the amplifier functions

The amplifier plays a critical roll as both protection circuitry as well as amplifying. The subsystem at high voltages will ensure a constant voltage output regardless how high the input voltages. However at much lower voltages it will use of pull/down resistors to ensure that the rpi doesnt run at critically low voltages

To get the most out of the user experience with the micro pihat, After having run the project from the git(@https://gitlab.com/group-361/ups-micro-pihat) you would need to build the circuit. When the circuit has been fully built the system has been built basically to be plug and play. However to get the most out of this the users would need to be careful. 

Once the hat has been pluged on top of the raspberry pi, the raspberry can be disconnected from the mains supply as now the pihat will power the rpi, connect the pihat back to the mains, when this happens the pihat will use primary power supply while the battery recharges if it was not full.

Do not use below 20 % charge of the battery, as this will be critical battery level.






