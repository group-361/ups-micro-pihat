EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "UPS Micro piHat Project Schematic"
Date "2021-06-11"
Rev "2.0"
Comp "Group 36"
Comment1 "This Schematic is licensed under MIT Open Source License."
Comment2 "Revision of version 1.0"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 5C77771F
P 6500 3200
F 0 "J1" H 6550 4317 50  0000 C CNN
F 1 "GPIO_CONNECTOR" H 6550 4226 50  0000 C CNN
F 2 "lib:PinSocket_2x20_P2.54mm_Vertical_Centered_Anchor" H 6500 3200 50  0001 C CNN
F 3 "~" H 6500 3200 50  0001 C CNN
	1    6500 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C777805
P 6100 4350
F 0 "#PWR04" H 6100 4100 50  0001 C CNN
F 1 "GND" H 6105 4177 50  0001 C CNN
F 2 "" H 6100 4350 50  0001 C CNN
F 3 "" H 6100 4350 50  0001 C CNN
	1    6100 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C777838
P 7000 4350
F 0 "#PWR05" H 7000 4100 50  0001 C CNN
F 1 "GND" H 7005 4177 50  0001 C CNN
F 2 "" H 7000 4350 50  0001 C CNN
F 3 "" H 7000 4350 50  0001 C CNN
	1    7000 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 2700 6100 2700
Wire Wire Line
	6100 2700 6100 3500
Wire Wire Line
	6300 3500 6100 3500
Connection ~ 6100 3500
Wire Wire Line
	6100 3500 6100 4200
Wire Wire Line
	6300 4200 6100 4200
Connection ~ 6100 4200
Wire Wire Line
	6100 4200 6100 4350
Wire Wire Line
	6800 2500 7000 2500
Wire Wire Line
	7000 2500 7000 2900
Wire Wire Line
	6800 2900 7000 2900
Connection ~ 7000 2900
Wire Wire Line
	7000 2900 7000 3200
Wire Wire Line
	6800 3200 7000 3200
Connection ~ 7000 3200
Wire Wire Line
	6800 3700 7000 3700
Wire Wire Line
	7000 3200 7000 3700
Connection ~ 7000 3700
Wire Wire Line
	7000 3700 7000 3900
Wire Wire Line
	6800 3900 7000 3900
Connection ~ 7000 3900
Wire Wire Line
	7000 3900 7000 4350
$Comp
L power:+3.3V #PWR03
U 1 1 5C777AB0
P 6050 2200
F 0 "#PWR03" H 6050 2050 50  0001 C CNN
F 1 "+3.3V" H 6065 2373 50  0000 C CNN
F 2 "" H 6050 2200 50  0001 C CNN
F 3 "" H 6050 2200 50  0001 C CNN
	1    6050 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2300 6050 2200
Wire Wire Line
	6300 3100 6050 3100
Wire Wire Line
	6050 3100 6050 2300
Connection ~ 6050 2300
Wire Wire Line
	6800 2300 6950 2300
Wire Wire Line
	6800 2400 7100 2400
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5C77824A
P 5650 2200
F 0 "#FLG01" H 5650 2275 50  0001 C CNN
F 1 "PWR_FLAG" H 5650 2374 50  0000 C CNN
F 2 "" H 5650 2200 50  0001 C CNN
F 3 "~" H 5650 2200 50  0001 C CNN
	1    5650 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5C778504
P 5700 4450
F 0 "#PWR02" H 5700 4200 50  0001 C CNN
F 1 "GND" H 5705 4277 50  0001 C CNN
F 2 "" H 5700 4450 50  0001 C CNN
F 3 "" H 5700 4450 50  0001 C CNN
	1    5700 4450
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5C778511
P 5700 4400
F 0 "#FLG02" H 5700 4475 50  0001 C CNN
F 1 "PWR_FLAG" H 5700 4574 50  0000 C CNN
F 2 "" H 5700 4400 50  0001 C CNN
F 3 "~" H 5700 4400 50  0001 C CNN
	1    5700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4400 5700 4450
Text Notes 7650 2400 0    50   ~ 10
If back powering Pi with 5V \nNOTE that the Raspberry Pi 3B+ and Pi Zero \nand ZeroW do not include an input ZVD.
Wire Notes Line
	7600 2100 7600 2450
Wire Notes Line
	7600 2450 9450 2450
Wire Notes Line
	9450 2450 9450 2100
Wire Notes Line
	9450 2100 7600 2100
Wire Wire Line
	6050 2300 6300 2300
Wire Wire Line
	5650 2300 5650 2200
Wire Wire Line
	5650 2300 6050 2300
Text Label 5350 2400 0    50   ~ 0
GPIO2_SDA1
Text Label 5350 2500 0    50   ~ 0
GPIO3_SCL1
Text Label 5350 2600 0    50   ~ 0
GPIO4_GPIO_GCLK
Text Label 5350 2800 0    50   ~ 0
GPIO17_GEN0
Text Label 5350 2900 0    50   ~ 0
GPIO27_GEN2
Text Label 5350 3000 0    50   ~ 0
GPIO22_GEN3
Text Label 5350 3200 0    50   ~ 0
GPIO10_SPI_MOSI
Wire Wire Line
	5250 3200 6300 3200
Wire Wire Line
	5250 3300 6300 3300
Wire Wire Line
	5250 3400 6300 3400
Wire Wire Line
	5250 3600 6300 3600
Wire Wire Line
	5250 3700 6300 3700
Wire Wire Line
	5250 3800 6300 3800
Wire Wire Line
	5250 3900 6300 3900
Wire Wire Line
	5250 4000 6300 4000
Wire Wire Line
	5250 4100 6300 4100
Wire Wire Line
	5250 3000 6300 3000
Wire Wire Line
	5250 2900 6300 2900
Wire Wire Line
	5250 2800 6300 2800
Wire Wire Line
	5250 2600 6300 2600
Wire Wire Line
	5250 2500 6300 2500
Wire Wire Line
	5250 2400 6300 2400
Text Label 5350 3300 0    50   ~ 0
GPIO9_SPI_MISO
Text Label 5350 3400 0    50   ~ 0
GPIO11_SPI_SCLK
Text Label 5350 3600 0    50   ~ 0
ID_SD
Text Label 5350 3700 0    50   ~ 0
GPIO5
Text Label 5350 3800 0    50   ~ 0
GPIO6
Text Label 5350 3900 0    50   ~ 0
GPIO13
Text Label 5350 4000 0    50   ~ 0
GPIO19
Text Label 5350 4100 0    50   ~ 0
GPIO26
NoConn ~ 5250 2400
NoConn ~ 5250 2500
NoConn ~ 5250 2600
NoConn ~ 5250 2800
NoConn ~ 5250 2900
NoConn ~ 5250 3000
NoConn ~ 5250 3200
NoConn ~ 5250 3300
NoConn ~ 5250 3400
NoConn ~ 5250 3600
NoConn ~ 5250 3700
NoConn ~ 5250 3800
NoConn ~ 5250 3900
NoConn ~ 5250 4000
NoConn ~ 5250 4100
Text Label 7150 2600 0    50   ~ 0
GPIO14_TXD0
Text Label 7150 2700 0    50   ~ 0
GPIO15_RXD0
Text Label 7150 2800 0    50   ~ 0
GPIO18_GEN1
Text Label 7150 3000 0    50   ~ 0
GPIO23_GEN4
Text Label 7150 3100 0    50   ~ 0
GPIO24_GEN5
Text Label 7150 3300 0    50   ~ 0
GPIO25_GEN6
Text Label 7150 3400 0    50   ~ 0
GPIO8_SPI_CE0_N
Text Label 7150 3500 0    50   ~ 0
GPIO7_SPI_CE1_N
Wire Wire Line
	6800 3400 7850 3400
Wire Wire Line
	6800 3500 7850 3500
Text Label 7150 3600 0    50   ~ 0
ID_SC
Text Label 7150 3800 0    50   ~ 0
GPIO12
Text Label 7150 4000 0    50   ~ 0
GPIO16
Text Label 7150 4100 0    50   ~ 0
GPIO20
Text Label 7150 4200 0    50   ~ 0
GPIO21
Wire Wire Line
	6800 2600 7850 2600
Wire Wire Line
	6800 2700 7850 2700
Wire Wire Line
	6800 2800 7850 2800
Wire Wire Line
	6800 3000 7850 3000
Wire Wire Line
	6800 3100 7850 3100
Wire Wire Line
	6800 3300 7850 3300
Wire Wire Line
	6800 3600 7850 3600
Wire Wire Line
	6800 3800 7850 3800
Wire Wire Line
	6800 4000 7850 4000
Wire Wire Line
	6800 4100 7850 4100
NoConn ~ 7850 2600
NoConn ~ 7850 2700
NoConn ~ 7850 2800
NoConn ~ 7850 3000
NoConn ~ 7850 3100
NoConn ~ 7850 3300
NoConn ~ 7850 3400
NoConn ~ 7850 3500
NoConn ~ 7850 3600
NoConn ~ 7850 3800
NoConn ~ 7850 4000
NoConn ~ 7850 4100
NoConn ~ 7850 4200
Wire Wire Line
	6800 4200 7850 4200
$Comp
L Mechanical:MountingHole H1
U 1 1 5C7C4C81
P 9500 2850
F 0 "H1" H 9600 2896 50  0000 L CNN
F 1 "MountingHole" H 9600 2805 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 9500 2850 50  0001 C CNN
F 3 "~" H 9500 2850 50  0001 C CNN
	1    9500 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C7C7FBC
P 9500 3050
F 0 "H2" H 9600 3096 50  0000 L CNN
F 1 "MountingHole" H 9600 3005 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 9500 3050 50  0001 C CNN
F 3 "~" H 9500 3050 50  0001 C CNN
	1    9500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5C7C8014
P 9500 3250
F 0 "H3" H 9600 3296 50  0000 L CNN
F 1 "MountingHole" H 9600 3205 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 9500 3250 50  0001 C CNN
F 3 "~" H 9500 3250 50  0001 C CNN
	1    9500 3250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5C7C8030
P 9500 3450
F 0 "H4" H 9600 3496 50  0000 L CNN
F 1 "MountingHole" H 9600 3405 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 9500 3450 50  0001 C CNN
F 3 "~" H 9500 3450 50  0001 C CNN
	1    9500 3450
	1    0    0    -1  
$EndComp
$Sheet
S 950  4450 2700 1350
U 60C112CF
F0 "leds" 50
F1 "leds.sch" 50
$EndSheet
$Sheet
S 900  2700 2750 1200
U 60BC177E
F0 "powerSupply" 50
F1 "powerSupply.sch" 50
$EndSheet
Text GLabel 6950 2300 2    50   Input ~ 0
Vrpi
Text GLabel 7100 2400 2    50   Output ~ 0
Vcharge
$Sheet
S 900  1050 2750 1300
U 60C95FED
F0 "Amplifier" 50
F1 "Amplifier.sch" 50
$EndSheet
$EndSCHEMATC
